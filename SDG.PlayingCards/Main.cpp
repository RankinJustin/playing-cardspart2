// Playing Cards
// Sawyer Grambihler
// Justin Rankin

#include <iostream>
#include <conio.h>

using namespace std;



enum class Suit {
	Diamonds,
	Hearts,
	Clubs,
	Spades
};

enum class Rank {
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

struct Card
{
	Suit Suit;
	Rank Rank;
};

void PrintCard(Card card) {
	switch (card.Rank)
	{
	case Rank::Two: cout << "two of";
		break;
	case Rank::Three: cout << "three of";
		break;
	case Rank::Four: cout << "four of";
		break;
	case Rank::Five: cout << "five of";
		break;
	case Rank::Six: cout << "six of";
		break;
	case Rank::Seven: cout << "seven of";
		break;
	case Rank::Eight: cout << "eight of";
		break;
	case Rank::Nine: cout << "nine of";
		break;
	case Rank::Ten: cout << "ten of";
		break;
	case Rank::Jack: cout << "jack of";
		break;
	case Rank::Queen: cout << "queen of";
		break;
	case Rank::King: cout << "king of";
		break;
	case Rank::Ace: cout << "ace of";
		break;
	}


	switch (card.Suit)
	{
	case Suit::Clubs: cout << " clubs";
		break;
	case Suit::Spades: cout << " spades";
		break;
	case Suit::Hearts: cout << " hearts";
		break;
	case Suit::Diamonds: cout << " diamonds";
		break;
	}
}

Card HighCard(Card card1, Card card2) {
	if (card1.Rank > card2.Rank)
	{
		return card1;
	}
	else if (card2.Rank > card1.Rank) 
	{
		return card2;

	}else if (card1.Rank == card2.Rank){
		if (card1.Suit > card2.Suit)
		{
			return card1;
		}
		else if (card2.Suit > card2.Suit) {
			return card2;
		}
	}
}

int main() {

	Card c1;
	c1.Rank = Rank::Six;
	c1.Suit = Suit::Spades;

	Card c2;
	c2.Rank = Rank::Ten;
	c2.Suit = Suit::Hearts;

	PrintCard(HighCard(c1, c2));
	
	(void)_getch();
	return 0;
}



